from functools import wraps

from django.conf import settings
from django.contrib import messages

from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import REDIRECT_FIELD_NAME

import requests

def check_recaptcha(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        request.recaptcha_is_valid = None
        if request.method == 'POST':
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            if result['success']:
                request.recaptcha_is_valid = True
            else:
                request.recaptcha_is_valid = False
                messages.error(request, 'Invalid reCAPTCHA. Please try again.')
        return view_func(request, *args, **kwargs)
    return _wrapped_view


def check_email_verification(user):
    """ Must have an EmailVerification model """
    #return EmailVerification.objects.all().filter(user=user, verified=True)
    pass


def check_email(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    actual_decorator = user_passes_test(
        check_email_verification,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator

def html_meta(view_func, title='Home', description='', author='', keywords=[]):
    """
    Decorator for HTML Meta Data
    """
    @wraps(view_func)
    def wrapper(request, *args, **kwargs):
        context = {
            'page_meta_title': title,
            'page_meta_description': description,
        }
        args = args + (context,)
        return view_func(request, *args, **kwargs)
    return wrapper

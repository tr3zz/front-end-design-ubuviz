import random
import string

DEFAULT_CHAR_STRING = string.ascii_lowercase + string.digits
DEFAULT_CHAR_INTS = string.digits


def generate_random_string(chars=DEFAULT_CHAR_STRING, size=6):
    return ''.join(random.choice(chars) for _ in range(size))


def generate_random_ints(chars=DEFAULT_CHAR_INTS, size=6):
    return ''.join(random.choice(chars) for _ in range(size))


def getornone(model, *args, **kwargs):
    ''' sometime we want to get one or none object '''
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return None

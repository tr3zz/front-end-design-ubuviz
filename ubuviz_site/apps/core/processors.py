from django.utils import timezone
from django.conf import settings

def year(request):
    """ Returns the YEAR """
    return {'YEAR': timezone.now().year}

from django.conf.urls import url
from .views import *

from django.contrib.flatpages import views as flatpages_views

# https://simpleisbetterthancomplex.com/references/2016/10/10/url-patterns.html

urlpatterns = [
    url(r'^$', Home, name='home'),

    url(r'^about/$', flatpages_views.flatpage, {'url': '/about/'}, name='about'),
    url(r'^terms/$', flatpages_views.flatpage, {'url': '/terms/'}, name='terms'),
    url(r'^privacy/$', flatpages_views.flatpage, {'url': '/privacy/'}, name='policy'),

    url(r'^contact/$', ContactCreate.as_view(), name='contact'),
    url(r'^contact/success/$', contact_success, name='contact_success'),

    url(r'^clients/$', ClientsSatisfiedView.as_view(), name='clients'),
    url(r'^services/$', ServicesView.as_view(), name='services'),
    url(r'^service/(?P<slug>[-\w]+)-(?P<pk>\d+)/$', ServiceDetailView.as_view(), name='service-detail'),
    url(r'^products/$', ProductsView.as_view(), name='products'),
    url(r'^product/(?P<slug>[-\w]+)-(?P<pk>\d+)/$', ProductDetailView.as_view(), name='product-detail'),
]

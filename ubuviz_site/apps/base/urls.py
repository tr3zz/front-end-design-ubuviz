from django.urls import path
from .views import *


app_name = "base"
urlpatterns = [
	path("products/", ProductListAPIView.as_view()),
	path("product/<int:pk>/", ProductDetailAPIView.as_view()),
	path("services/", ServiceListAPIView.as_view()),
	path("service/<int:pk>/", ServiceListAPIView.as_view()),
    path("contact/", ContactCreateAPIView.as_view()),
    path("clients/", ClientListAPIView.as_view()),
    path("team/", TeamListAPIView.as_view()),
    path("text/<str:unique_id>/", TextModelDetailAPIView.as_view()),
]

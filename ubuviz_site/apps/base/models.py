from django.db import models
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField

from ubuviz_site.apps.core.models import TimestampedModel

from django.urls import reverse
from django.contrib.sitemaps import ping_google


def product_directory_path(instance, filename):
    return 'products/p_{0}_{1}_{2}'.format(instance.id, instance.slug, filename)

def team_directory_path(instance, filename):
    return 'team/p_{0}_{1}_{2}'.format(instance.id, instance.slug, filename)

def client_directory_path(instance, filename):
    return 'clients/p_{0}_{1}_{2}'.format(instance.id, instance.slug, filename)


class NameSlugDescriptionModel(TimestampedModel):
    name = models.CharField(_('Name'), max_length=250, unique=True)
    slug = models.SlugField(_('Slug'), unique=True)
    description = RichTextField(blank=True)

    class Meta(TimestampedModel.Meta):
        abstract = True

    def __str__(self):
        return self.name


class Contact(TimestampedModel):
	name = models.CharField(_('Name'), max_length=150)
	email = models.EmailField(_('Email'))
	message  = models.TextField(_('Message'))

	def __str__(self):
		return "%s (%s)" % (self.name, self.email)


#class Service(NameSlugDescriptionModel):
#    fa_icon = models.CharField(max_length=50)

#    def get_absolute_url(self):
#        return reverse('base:service-detail', kwargs={'pk': self.pk, 'slug': self.slug})

#    def __str__(self):
#        return self.name


class Product(NameSlugDescriptionModel):
    small_text = models.TextField(help_text=_("REQUIRED! Small Description of a product... Don't write too much nor Too Small... Two or Three paragraphs maybe."),  blank=True, null=True)

    picture = models.ImageField(upload_to=product_directory_path, null=True, blank=True)
    card_color = models.CharField(max_length=50, null=True, blank=True)

    external_link = models.URLField(blank=True, null=True)

    is_a_service = models.BooleanField(default=False, help_text=_("Specify if the Product will be promt as a service..."))

    fa_icon = models.CharField(max_length=50, null=True, blank=True, help_text=_("This will be used to set a font-awesome icon to a product wich is set as a service. We use Font Awesome 5. Just put the icon name for example just put 'user' for the icon 'fas fa-user"))

    is_active = models.BooleanField(default=True, help_text=_("It is not recommended to delete a product... You can only deactivate it..."))

    is_feature = models.BooleanField(default=False, help_text=_("Specify if the product is a feature"))

    is_home_slider_item = models.BooleanField(default=False, help_text=_("Check this if the product is to appear as a Home Slider Item."))

    def get_absolute_url(self):
        if self.is_a_service:
            return reverse('base:service-detail', kwargs={'pk': self.pk, 'slug': self.slug})
        else:
            return reverse('base:product-detail', kwargs={'pk': self.pk, 'slug': self.slug})

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False):
        super(Product, self).save(force_insert, force_update)
        try:
            ping_google()
        except Exception:
            pass


class Team(NameSlugDescriptionModel):
    job_title = models.TextField(blank=True, null=True)
    work_order = models.IntegerField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    facebook_link = models.URLField(null=True, blank=True)
    twitter_link = models.URLField(null=True, blank=True)
    google_plus_link = models.URLField(null=True, blank=True)
    linked_in_link = models.URLField(null=True, blank=True)
    github_link = models.URLField(null=True, blank=True)
    picture = models.ImageField(blank=True, null=True, upload_to=team_directory_path)
    is_active = models.BooleanField(default=True)

    class Meta(NameSlugDescriptionModel.Meta):
        ordering = ['work_order']

    def save(self, force_insert=False, force_update=False):
        super(Team, self).save(force_insert, force_update)
        try:
            ping_google()
        except Exception:
            pass


class Client(NameSlugDescriptionModel):
    external_link = models.URLField(blank=True, null=True)
    satisfied = models.BooleanField(default=True)
    picture = models.ImageField(blank=True, null=True, upload_to=client_directory_path)

    def save(self, force_insert=False, force_update=False):
        super(Client, self).save(force_insert, force_update)
        try:
            ping_google()
        except Exception:
            pass


class TextObjectModel(NameSlugDescriptionModel):
    unique_id = models.CharField(max_length=20, unique=True)

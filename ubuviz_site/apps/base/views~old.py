from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic.edit import CreateView
from django.views.generic import ListView, DetailView, TemplateView
from django.views import View
from .models import Contact, Product, Client

from braces.views import FormMessagesMixin

from ubuviz_site.apps.core.mixins import PageHtmlMetaMixin
from ubuviz_site.apps.core.decorators import html_meta

from .forms import ContactForm

# Create your views here.

def Home(request):
    page_meta_title = _('Home')
    page_meta_title_append = 'UbuViz'

    products = Product.objects.filter(is_a_service=False, is_feature=False, is_active=True)
    services = Product.objects.filter(is_a_service=True, is_feature=False, is_active=True)
    sliders  = Product.objects.filter(is_home_slider_item=True, is_feature=False, is_active=True)
    features = Product.objects.filter(is_feature=True, is_active=True)
    contact_form = ContactForm()

    return render(request, 'layouts/app.html', locals())

''' J'ai laisser l'utilisation d'un class view parce que je le maitrise pas et j'ai utiliser
    une fonction je crois que tu peux ajuster par ce que tu prefere
    en fait j'ai fait ca pour similer une base de donnees et de pouvoir afficher les donnees avec Jinja
    // Muco Tresor
 '''

 # class Home(PageHtmlMetaMixin, TemplateView):
 #     """ Home Page """
 #     template_name = 'layouts/app.html'
 #     page_meta_title = _('Home')
 #     page_meta_title_append = 'UbuViz'
 #    def get_context_data(self, *args, **kwargs):
 #            context = super(Home, self).get_context_data(*args, **kwargs)
 #            context['services'] = Product.objects.filter(is_a_service=True, is_active=True)
 #            return context


class ServicesView(PageHtmlMetaMixin, ListView):
    queryset = Product.objects.filter(is_a_service=True, is_active=True)
    context_object_name = 'services'
    template_name = 'base/service_list.html'
    page_meta_title = _('Services')


class ServiceDetailView(PageHtmlMetaMixin, DetailView):
    queryset = Product.objects.filter(is_a_service=True, is_active=True)
    context_object_name = 'service'
    template_name = 'base/service_detail.html'

    def get_page_meta_title(self):
        return "%s | Service" % self.object.name

    def get_page_meta_tdescription(self):
        return "'%s', %s" % (self.object.name, self.object.description)


class ProductsView(PageHtmlMetaMixin, ListView):
    queryset = Product.objects.filter(is_a_service=False, is_active=True)
    context_object_name = 'products'
    page_meta_title = _('Products')
    page_meta_description = _('All Ubuviz Products.')


class ProductDetailView(PageHtmlMetaMixin, DetailView):
    queryset = Product.objects.filter(is_a_service=False, is_active=True)
    context_object_name = 'product'

    def get_page_meta_title(self):
        return "%s | Product" % self.object.name

    def get_page_meta_tdescription(self):
        return "'%s', %s" % (self.object.name, self.object.description)


def contact_success(request):
    """ Contact Success Page """
    page_title = "contact"
    page_meta_description = _('Contact Sent Successfully to the Ubuviz Team.')
    return render(request, 'base/contact_success.html', locals())


class ContactCreate(PageHtmlMetaMixin, FormMessagesMixin, CreateView):
    model = Contact
    fields = '__all__'
    success_url = reverse_lazy('base:home')
    page_meta_title = _('Contact')
    page_meta_description = _('Have a question, a request, an idea that can help or just a message? Contact the Ubuviz Team.')
    form_valid_message = "Contact sent!"
    form_invalid_message = "Contact not sent!"

    def get_context_data(self, **kwargs):
        context = super(ContactCreate, self).get_context_data(**kwargs)
        context['page_title'] = 'contact'

        return context


class ClientsSatisfiedView(PageHtmlMetaMixin, ListView):
    queryset = Client.objects.filter(satisfied=True)
    context_object_name = 'clients'
    page_meta_title = _('Clients')
    page_meta_description = _('Client Satisfied by Ubuviz Services all over the world.')

from rest_framework import mixins
from rest_framework import generics

from .serializers import *
from .models import *


class ProductListAPIView(mixins.ListModelMixin,
                    generics.GenericAPIView):
    queryset = Product.objects.filter(is_a_service=False, is_active=True)
    serializer_class = ProductSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ServiceListAPIView(mixins.ListModelMixin,
                    generics.GenericAPIView):
    queryset = Product.objects.filter(is_a_service=True, is_active=True)
    serializer_class = ProductSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ProductDetailAPIView(mixins.RetrieveModelMixin,
                            generics.GenericAPIView):
    queryset = Product.objects.filter(is_a_service=False, is_active=True)
    serializer_class = ProductSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class ServiceDetailAPIView(mixins.RetrieveModelMixin,
                            generics.GenericAPIView):
    queryset = Product.objects.filter(is_a_service=True, is_active=True)
    serializer_class = ProductSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class TeamListAPIView(mixins.ListModelMixin,
                            generics.GenericAPIView):
    queryset = Team.objects.filter(is_active=True)
    serializer_class = TeamSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ContactCreateAPIView(mixins.CreateModelMixin,
                            generics.GenericAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class TextModelDetailAPIView(mixins.RetrieveModelMixin,
                            generics.GenericAPIView):
    queryset = TextObjectModel.objects.all()
    serializer_class = TextObjectSerializer
    lookup_field = 'unique_id'

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class ClientListAPIView(mixins.ListModelMixin,
                            generics.GenericAPIView):
    queryset = Client.objects.filter(satisfied=True)
    serializer_class = ClientSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

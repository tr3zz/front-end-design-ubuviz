from .models import Product, Team, Client
from django.utils.translation import ugettext as _
from django.utils import timezone
from django.conf import settings

def default_models(request):
    """ Returns the TMT API Version """
    return {
        'SERVICES': Product.objects.filter(is_a_service=True, is_active=True),
        'PRODUCTS': Product.objects.filter(is_a_service=False, is_active=True),
        'CAROUSELS': Product.objects.filter(is_home_slider_item=True, is_active=True),
        'CLIENTS': Client.objects.filter(satisfied=True),
        'TEAM': Team.objects.all(),
    }

def ubuviz(request):
    """ Returns the TMT API Version """
    return {
        'UBUVIZ_DEVISE': "Developing Apps",
        'UBUVIZ_FACEBOOK': 'https://www.facebook.com/TechViz.io/',
        'UBUVIZ_TWITTER': 'https://twitter.com/UbuViz',
        'UBUVIZ_INSTAGRAM': 'https://www.instagram.com/Ubuviz/',
        'UBUVIZ_LINKEDIN': 'https://www.linkedin.com/company/ubuviz',
        'UBUVIZ_GOOGLE_PLUS': 'https://www.googleplus.com/Ubuviz/',
        'UBUVIZ_EMAIL': 'info@ubuviz.com',
        'UBUVIZ_DESCRIPTION': getattr(settings, 'PAGE_META_DEFAULT_DESCRIPTION', 'UbuViz - Burundi, IT Solutions.'),
        'UBUVIZ_PHONES': ['+25761200233'],
        'UBUVIZ_ADDRESS': '',
        'UBUVIZ_ROAD': '76, Boulevard du 1er Novembre',
        'UBUVIZ_HOUSE': 'Immeuble Peace Corner, C6',
        'UBUVIZ_QUARTER': 'Asiatique - Mukaza',
        'YEAR': timezone.now().year,
        'DEBUG': settings.DEBUG,
    }

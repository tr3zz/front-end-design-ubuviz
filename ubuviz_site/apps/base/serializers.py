from rest_framework import serializers

from .models import Product, Contact, Team, TextObjectModel, Client


class ProductSerializer(serializers.ModelSerializer):
    sigle = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Product
        fields = [
            "name", "small_text", "picture", "external_link", "is_a_service", "fa_icon", "is_active",
            "is_feature", "is_home_slider_item", "description", "sigle", "card_color",
        ]

    def get_sigle(self, obj):
        name = obj.name
        name_tab = name.split(" ")
        res = name
        if len(name_tab) >= 2:
            res = ""
            for i in name_tab:
                res += i[0]
        return res

class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ["name", "email", "message"]


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = [
            "work_order", "job_title", "email", "facebook_link", "twitter_link", "google_plus_link", "linked_in_link",
            "github_link", "picture", "is_active", "name", "description",
        ]


class TextObjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextObjectModel
        fields = [
            "name", "slug", "description", "unique_id",
        ]


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = [
            "name", "external_link", "picture",
        ]

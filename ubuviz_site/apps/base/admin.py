from django.contrib import admin
from django.contrib.flatpages.models import FlatPage

from django import forms

# Note: we are renaming the original Admin and Form as we import them!
from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld

from ckeditor.widgets import CKEditorWidget
from .models import *


class FlatpageForm(FlatpageFormOld):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta(FlatpageFormOld.Meta):
        pass


class FlatPageAdmin(FlatPageAdminOld):
    form = FlatpageForm

# ---------- BASE APP ---------
class TextObjectModelAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'is_a_service', 'is_feature', 'is_home_slider_item')


class TeamAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class ClientAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class ContactAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'created_at',)


# We have to unregister the normal admin, and then reregister ours
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)

#Models
admin.site.register(TextObjectModel, TextObjectModelAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Team, TeamAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Contact, ContactAdmin)

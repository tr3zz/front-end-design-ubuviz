from django.contrib import sitemaps
from django.urls import reverse

from .apps.base.models import *


class MainSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ['home']

    def location(self, item):
        return reverse(item)


class ProductSitemap(sitemaps.Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Product.objects.filter(is_a_service=False, is_active=True)

    def lastmod(self, obj):
        return obj.created_at


class ServiceSitemap(sitemaps.Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Product.objects.filter(is_a_service=True, is_active=True)

    def lastmod(self, obj):
        return obj.created_at


class TeamSitemap(sitemaps.Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Team.objects.filter(is_active=True)

    def lastmod(self, obj):
        return obj.created_at


class ClientSitemap(sitemaps.Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Client.objects.filter(is_active=True)

    def lastmod(self, obj):
        return obj.created_at


ubuviz_sitemaps = {
    'main': MainSitemap,
    'products': ProductSitemap,
    'services': ServiceSitemap,
    #'team': Team,
}
